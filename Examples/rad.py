#!/usr/bin/python

import sys
import os
import glob
from DiscreteFunction import *
from WaveFunction import *
from DataAnalysis import *
from RadiatedQuantities import *
import matplotlib.pyplot as plt

usage_msg = """
USAGE: rad.py <dir>

Computes radiated quantities.

"""


if (len(sys.argv) < 2):
    sys.exit(usage_msg)    


directory=sys.argv[1]

lmax = 2

Modes = LoadModeArray(directory+"/newsb", lmax)

IntModes = InitModeArray(lmax)
for ll in range(2,lmax+1):
    for mm in range(-ll, ll+1):
	IntModes[ll][mm] = Modes[ll][mm].IntegrateFunction()
	IntModes[ll][mm] = ShiftToZero(IntModes[ll][mm], [1000,1100])

Edot = RadiatedEnergyFlux(Modes, lmax)

Lxdot, Lydot, Lzdot = RadiatedAngMomFlux(Modes, IntModes, lmax)


print Lzdot